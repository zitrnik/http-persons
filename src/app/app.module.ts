import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgxMaskModule, IConfig } from "ngx-mask";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { PersonAddComponent } from "./person-add/person-add.component";
import { PersonViewComponent } from "./person-view/person-view.component";
import { SearchPipePipe } from './pipes/search-pipe.pipe';
import { HttpClientModule } from "@angular/common/http";

export let options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [AppComponent, PersonAddComponent, PersonViewComponent, SearchPipePipe],
  imports: [
    BrowserModule,
    NgxMaskModule.forRoot(options),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
