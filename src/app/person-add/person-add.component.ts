import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import Person from '../shared/models/person.model';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { PostServiceService } from '../shared/services/post-service.service';

@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {
  constructor(private postsAPI: PostServiceService) {}
  @Output() addPerson = new EventEmitter<Person>();
  newUser: FormGroup;
  dis = false;

  ngOnInit() {
    this.newUser = new FormGroup({
      name: new FormControl({ value: "", disabled: false }, [
        Validators.required
      ]),
      surname: new FormControl({ value: "", disabled: false }, [
        Validators.required
      ]),
      phone: new FormControl({ value: "" }, [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ])
    });
  }
  async getLastData() {
    try {
      const res = await this.postsAPI.getPosts();
      console.log('type');
      console.log(typeof res);
      return res;
    } catch (e) {
      console.error(e);
    }
  }
  async postData(data) {
    console.log('apppost');
    console.log(data);
    try {
      let a = await this.postsAPI.postPosts(data);
    } catch (e) {
      console.error(e);
    }
  }
  onAddPerson() {
    if (typeof this.newUser.value.phone !== 'object') {
      let person = new Person(
        this.newUser.value.name,
        this.newUser.value.surname,
        "+7" + this.newUser.value.phone
      );
      let obj = this.getLastData();
      let last;
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          last = obj[key];
        }
      }
      // console.log(obj);
      let post = {
          id: last.id+1,
          name: this.newUser.value.name,
          surname: this.newUser.value.surname,
          phone: '+7'+this.newUser.value.phone
        }
      this.postData(post);
      this.newUser.reset();
      this.addPerson.emit(person);
    }
  }
}
