import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiInteractService {
  private baseURL = "http://localhost:3000/";
  // tslint:disable-next-line: no-shadowed-variable
  constructor(public HttpClient: HttpClient) {}
  private getUrl(url) {
    return this.baseURL + url;
  }
  public get(url: string, header?: HttpHeaders) {
    let requestOptions = {
      headers: header
    };
    return this.HttpClient.get(this.getUrl(url), requestOptions);
  }
  public post(url: string, data: any, header: HttpHeaders) {
    let requestOptions = {
      headers: header
    };
    console.log('apipost');
    console.log(data);
    return this.HttpClient.post(this.getUrl(url), data, requestOptions);
  }
  public put(url: string, data: any, header: HttpHeaders,) {
    console.log('putApi');
    console.log(data);
    let requestOptions = {
      headers: header
    };
    return this.HttpClient.put(this.getUrl(url), data, requestOptions);
  }
  public delete(url: string, header: HttpHeaders) {
    let requestOptions = {
      headers: header
    };
    return this.HttpClient.delete(this.getUrl(url), requestOptions);
  }
}
